<?php

namespace UnitTests;

use App\Entity\Order;
use App\Repository\OrderRepository;
use App\Service\OrderService;
use PHPUnit\Framework\TestCase;

class OrdersServiceTest extends TestCase
{
    public function testCreateOrderWithValidData(): void
    {
        $data = [
            'customerId' => 1,
            'products' => [123456, 789456]
        ];

        $orderRepository = $this->createMock(OrderRepository::class);
        $orderRepository->expects($this->once())
            ->method('createOrder')
            ->with($this->isInstanceOf(Order::class));

        $orderService = new OrderService($orderRepository);
        $order = $orderService->createOrder($data);

        $this->assertInstanceOf(Order::class, $order);
        $this->assertEquals($data['customerId'], $order->getCustomerId());
        $this->assertEquals($data['products'], $order->getProducts());
    }

    public function testCreateOrderWithMissingCustomerId(): void
    {
        $data = [
            'orders' => ['order1', 'order2']
        ];

        $orderRepository = $this->createMock(OrderRepository::class);
        $orderService = new OrderService($orderRepository);

        $this->expectException(\InvalidArgumentException::class);
        $orderService->createOrder($data);
    }

    public function testCreateOrderWithMissingOrders(): void
    {
        $data = [
            'customerId' => 1
        ];

        $orderRepository = $this->createMock(OrderRepository::class);
        $orderService = new OrderService($orderRepository);

        $this->expectException(\InvalidArgumentException::class);
        $orderService->createOrder($data);
    }

    public function testCreateOrderWithoutData(): void
    {
        $data = [];

        $orderRepository = $this->createMock(OrderRepository::class);
        $orderService = new OrderService($orderRepository);

        $this->expectException(\InvalidArgumentException::class);
        $orderService->createOrder($data);
    }

    public function testOrderUpdateWithNonExistentOrder(): void
    {
        $data = [
            'customerId' => 2,
            'orders' => ['order3', 'order4']
        ];
        $id = 999;

        $orderRepository = $this->createMock(OrderRepository::class);
        $orderRepository->method('findOneBy')
            ->willReturn(null);

        $orderService = new OrderService($orderRepository);
        $result = $orderService->updateOrder($data, $id);

        $this->assertFalse($result);
    }

    public function testOrderUpdateWithMissingData(): void
    {
        $data = [];
        $id = 1;

        $orderRepository = $this->createMock(OrderRepository::class);
        $orderService = new OrderService($orderRepository);
        $order = $orderService->updateOrder($data, $id);

        $this->assertFalse($order);
    }

    public function testOrderUpdateWithOrders(): void
    {
        $mockOrder = new Order();
        $mockOrder->setProducts(['product1', 'product2']);
        $mockOrder->setCustomerId(1);

        $orderRepository = $this->createMock(OrderRepository::class);
        $orderRepository->method('findOneBy')
            ->willReturn($mockOrder);

        $orderService = new OrderService($orderRepository);

        $dataUpdate = ['products' => ['product3', 'product4']];

        $order = $orderService->updateOrder($dataUpdate, 1);

        $this->assertEquals($dataUpdate['products'], $order->getProducts());
    }

    public function testOrderDeleteWithNonExistentOrder(): void
    {
        $id = 999;

        $orderRepository = $this->createMock(OrderRepository::class);
        $orderRepository->method('find')
            ->willReturn(null);

        $orderService = new OrderService($orderRepository);
        $result = $orderService->deleteOrder($id);

        $this->assertFalse($result);
    }

    public function testOrderDeleteWithId(): void
    {
        $mockOrder = new Order();
        $mockOrder->setProducts(['order1', 'order2']);
        $mockOrder->setCustomerId(1);

        $orderRepository = $this->createMock(OrderRepository::class);
        $orderRepository->method('find')
            ->willReturn($mockOrder);

        $orderService = new OrderService($orderRepository);

        $result = $orderService->deleteOrder(1);

        $this->assertTrue($result);
    }

}
