<?php

namespace App\Service;

use App\Entity\Order;
use App\Repository\OrderRepository;
use Pagerfanta\Doctrine\ORM\QueryAdapter;
use Pagerfanta\Pagerfanta;

class OrderService
{
    private OrderRepository $orderRepository;

    public function __construct(OrderRepository $orderRepository)
    {
        $this->orderRepository = $orderRepository;
    }

    public function getAllOrders(int $page = 1, int $limit = 10): Pagerfanta
    {
        $queryBuilder = $this->orderRepository->createQueryBuilder('o')
            ->orderBy('o.id', 'ASC');

        $adapter = new QueryAdapter($queryBuilder);
        $pagerfanta = new Pagerfanta($adapter);
        $pagerfanta->setMaxPerPage($limit);
        $pagerfanta->setCurrentPage($page);

        return $pagerfanta;
    }

    public function getOrder(int $id)
    {
        return $this->orderRepository->findOneBy(['id' => $id]);
    }


    public function createOrder($data): Order
    {
        $order = new Order();
        $order->setCreatedAt(new \DateTimeImmutable());
        if (!isset($data['customerId']) || !isset($data['products'])) {
            throw new \InvalidArgumentException('Missing required data');
        }
        $order->setCustomerId($data['customerId']);
        $order->setProducts($data['products']);

        $this->orderRepository->createOrder($order);
        return $order;

    }

    public function updateOrder($data, $id)
    {
        $order = $this->orderRepository->findOneBy(["id" => $id]);

        if (!$order) {
            return false;
        }

        if (isset($data['products'])) {
            $order->setProducts($data['products']);
            $this->orderRepository->update($order);

        }

        return $order;
    }


    public function deleteOrder($id): bool
    {
        $order = $this->orderRepository->find($id);
        if (!$order) {
            return false;
        }
        $this->orderRepository->delete($order);
        return true;
    }
}
