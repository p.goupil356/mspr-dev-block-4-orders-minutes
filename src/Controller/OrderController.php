<?php

namespace App\Controller;

use App\Service\OrderService;
use OpenApi\Annotations as OA;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Serializer\SerializerInterface;

class OrderController extends AbstractController
{
    /**
     * @Route("/api/v1/orders", name="order_index", methods={"GET"})
     * @OA\Get(
     *     path="/api/v1/orders",
     *     summary="Get all orders",
     *     tags={"Orders"},
     *     @OA\Response(
     *         response=200,
     *         description="Successful operation",
     *         @OA\JsonContent(type="array", @OA\Items(ref=@Model(type=App\Entity\Order::class, groups={"order:read"})))
     *     )
     * )
     */
    #[Route('/api/v1/orders', name: 'order_index', methods: ['GET'])]
    public function index(Request $request, OrderService $orderService, SerializerInterface $serializer): JsonResponse
    {
        $page = $request->query->getInt('page', 1);
        $limit = $request->query->getInt('limit', 10);

        $orders = $orderService->getAllOrders($page, $limit);

        $data = [
            'items' => iterator_to_array($orders->getCurrentPageResults()),
            'current_page' => $orders->getCurrentPage(),
            'total_items' => $orders->getNbResults(),
            'total_pages' => $orders->getNbPages(),
        ];

        $json = $serializer->serialize($data, 'json');

        return new JsonResponse($json, 200, [], true);
    }

    /**
     * @Route("/api/v1/orders/{id}", name="order_show", methods={"GET"})
     * @OA\Get(
     *     path="/api/v1/orders/{id}",
     *     summary="Get an order by ID",
     *     tags={"Orders"},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="Order ID",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Successful operation",
     *         @OA\JsonContent(ref=@Model(type=App\Entity\Order::class, groups={"order:read"}))
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Order not found"
     *     )
     * )
     */
    #[Route('/api/v1/orders/{id}', name: 'order_show', methods: ['GET'])]
    public function show(int $id, OrderService  $orderService, SerializerInterface $serializer): JsonResponse
    {
        $order = $orderService->getOrder($id);
        if (!$order) {
            return $this->json(['message' => 'Order not found'], 404);
        }
        $data = $serializer->serialize($order, 'json', ['groups' => 'order:read']);

        return new JsonResponse($data, 200, [], true);    }

    /**
     * @Route("/api/v1/orders", name="order_create", methods={"POST"})
     * @OA\Post(
     *     path="/api/v1/orders",
     *     summary="Create a new order",
     *     tags={"Orders"},
     *     @OA\RequestBody(
     *         description="Order object that needs to be added",
     *         required=true,
     *         @OA\JsonContent(ref=@Model(type=App\Entity\Order::class))
     *     ),
     *     @OA\Response(
     *         response=201,
     *         description="Order created",
     *         @OA\JsonContent(ref=@Model(type=App\Entity\Order::class))
     *     )
     * )
     */
    #[Route('/api/v1/orders', name: 'order_create', methods: ['POST'])]
    public function create(Request $request, OrderService  $orderService): JsonResponse
    {
        $data = json_decode($request->getContent(), true);
        $order = $orderService->createOrder($data);

        return $this->json($order, 201);
    }

    /**
     * @Route("/api/v1/orders/{id}", name="order_update", methods={"PUT", "PATCH"})
     * @OA\Put(
     *     path="/api/v1/orders/{id}",
     *     summary="Update an existing order",
     *     tags={"Orders"},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="Order ID",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\RequestBody(
     *         description="Order object that needs to be updated",
     *         required=true,
     *         @OA\JsonContent(ref=@Model(type=App\Entity\Order::class))
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Order updated",
     *         @OA\JsonContent(ref=@Model(type=App\Entity\Order::class))
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Order not found"
     *     )
     * )
     * @OA\Patch(
     *     path="/api/v1/orders/{id}",
     *     summary="Partially update an existing order",
     *     tags={"Orders"},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="Order ID",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\RequestBody(
     *         description="Order object that needs to be partially updated",
     *         required=true,
     *         @OA\JsonContent(ref=@Model(type=App\Entity\Order::class))
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Order updated",
     *         @OA\JsonContent(ref=@Model(type=App\Entity\Order::class))
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Order not found"
     *     )
     * )
     */
    #[Route('/api/v1/orders/{id}', name: 'order_update', methods: ['PUT', 'PATCH'])]
    public function update( int $id, Request $request, OrderService $orderService): JsonResponse
    {
        $order = $orderService->updateOrder(json_decode($request->getContent(), true), $id);

        if (!$order) {
            return $this->json(['message' => 'Order not found'], 404);
        }

        return $this->json($order, 201);
    }

    /**
     * @Route("/api/v1/orders/{id}", name="order_delete", methods={"DELETE"})
     * @OA\Delete(
     *     path="/api/v1/orders/{id}",
     *     summary="Delete an order by ID",
     *     tags={"Orders"},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="Order ID",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Order deleted",
     *         @OA\JsonContent(
     *             type="object",
     *             @OA\Property(property="message", type="string", example="Order deleted")
     *         )
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Order not found"
     *     )
     * )
     */
    #[Route('/api/v1/orders/{id}', name: 'order_delete', methods: ['DELETE'])]
    public function delete(int $id, OrderService $orderService): JsonResponse
    {
        $order = $orderService->deleteOrder($id);
        if (!$order) {
            return $this->json(['message' => 'Order not found'], 404);
        }

        return $this->json(['message' => 'Order deleted']);
    }


}
